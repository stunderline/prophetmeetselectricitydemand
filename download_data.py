import pandas as pd
import requests


BASE_URL = 'https://www.swissgrid.ch/dam/dataimport/energy-statistic/'
AVAILABLE_YEARS = range(2009,2020)
SHEET_NAME = "Zeitreihen0h15"

COLUMN_NAMES = {
    'demand' : "Summe endverbrauchte Energie Regelblock Schweiz\nTotal energy consumed by end users in the Swiss controlblock",
    'production' : "Summe produzierte Energie Regelblock Schweiz\nTotal energy production Swiss controlblock"
}

def download_data():
    """Downloads the .xls files for all available years and saves to local directory."""
    for year in AVAILABLE_YEARS:
        filename = f'EnergieUebersichtCH-{year}.xls'
        url = f'{BASE_URL}/{filename}'
        print(f'Downloading data for year {year} from {url}')
        web_file = requests.get(url)
        with open(filename, 'wb') as local_file:
            local_file.write(web_file.content)


def extract_ts(what: str='demand'):
    """Extract a single 15 minute time series. Can be either 'demand' or 'production'.
    Saves combined file to local directory with filename {what}.csv .
    """
    col_name = COLUMN_NAMES[what]
    series = []
    for year in AVAILABLE_YEARS:
        print(f'Processing {year}...')
        filename = f'EnergieUebersichtCH-{year}.xls'
        df_single_year = pd.read_excel(filename, sheet_name=SHEET_NAME, header=0)
        df_single_year = df_single_year[col_name][1:] #skip first row following the column names
        series.append(df_single_year)
    series = pd.concat(series)
    series = series.rename(what)
    series.to_csv(f'{what}.csv', index=True, header=True)


if __name__ == '__main__':
    download_data()
    extract_ts('demand')
    extract_ts('production')

