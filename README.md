# ProphetMeetsElectricityDemand

Code for a blogpost on using Prophet to forecast Switzerland's electricity demand.


# Downloading the data
run `python download_data.py`

# running sample analysis
run `python analyze_data.py`


# Requirements
python >= 3.6

fbprophet

pandas

matplotlib

requests