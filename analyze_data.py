import pandas as pd
from fbprophet import Prophet
from fbprophet.diagnostics import cross_validation
from fbprophet.diagnostics import performance_metrics
from fbprophet.plot import plot_cross_validation_metric
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams['figure.figsize'] = (12.0, 6.0)
pd.plotting.register_matplotlib_converters() # see https://stackoverflow.com/questions/43206554/typeerror-float-argument-must-be-a-string-or-a-number-not-period


def load_data(what: str='demand') -> pd.DataFrame:
    df = pd.read_csv(f'{what}.csv', names=['ds','y'], header=0)
    df['ds'] = pd.to_datetime(df['ds'])
    df = df.set_index('ds')
    return df

def resample_daily(df: pd.DataFrame) -> pd.DataFrame:
    df = df.resample('D', closed='right').sum()
    return df

def prophet(df: pd.DataFrame, add_holidays: bool=False):
    if add_holidays:
        holidays = '_holidays'
    else:
        holidays = ''
    df = df.reset_index()
    m = Prophet()
    if add_holidays:
        m.add_country_holidays(country_name='Switzerland')
    m.fit(df)
    future = m.make_future_dataframe(periods=365)
    forecast = m.predict(future)

    ax = m.plot(forecast)
    plt.savefig(f'prophet_daily_forecast{holidays}.png')
    
    fig2 = m.plot_components(forecast)
    plt.savefig(f'prophet_daily_components{holidays}.png')

    df_cv = cross_validation(m, initial=f'{5*365} days', period='90 days', horizon = '365 days')
    df_metrics = performance_metrics(df_cv)

    fig3 = plot_cross_validation_metric(df_cv, metric='mse')
    plt.savefig(f'prophet_daily_mse{holidays}.png')
    fig4 = plot_cross_validation_metric(df_cv, metric='mape')
    plt.savefig(f'prophet_daily_mape{holidays}.png')

    plt.show()

if __name__ == "__main__":
    df = load_data('demand')

    ax = df.plot()
    ax.set_xlabel('timestamp')
    ax.set_ylabel('energy consumption [kWh]')
    ax.set_title('end-user energy consumption over 15-minute intervals')
    ax.get_legend().remove()

    daily = resample_daily(df)
    ax = daily.plot()
    ax.set_xlabel('timestamp')
    ax.set_ylabel('energy consumption [kWh]')
    ax.set_title('end-user energy consumption per day')
    ax.get_legend().remove()
    plt.savefig('raw_daily_resolution.png')
    plt.show()
    
    prophet(daily, add_holidays=True)

